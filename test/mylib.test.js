const expect = require('chai').expect;
const assert = require('chai').assert;
const should = require('chai').should();
const mylib = require('../src/mylib');

describe('src/mylib.js', () => {

    let component = undefined;

    before(() => {
        console.log('before');
        component = 1;
    });
    after(() => {
        console.log('after');
        conponent = undefined;
    });

    it('test that component exists', async () => {
        should.exist(component);
    });

    describe('/add', () => {
        it('happy scenario: pass two numbers, get their sum', async () => {
            const result = mylib.sum(1,2);
            expect(result).to.equal(3);
        });
        
        it('test that function works with negative amounts', async () => {
            const result = mylib.sum(-10,-2);
            expect(result).to.equal(-12);
        });
    });

    describe('/sub', () => {
        it('happy scenario: pass two numbers, get their difference', async () => {
            const result = mylib.subtract(10,3);
            expect(result).to.equal(7);
        });

        it('test that function works with negative amounts', async () => {
            const result = mylib.subtract(-10,-2);
            expect(result).to.equal(-8);
        });
    });

});