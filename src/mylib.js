module.exports = {

/**
 * Add two numbers together
 * @param {Number} a
 * @param {Number} b
 * @returns {Number} sum of a and b
 */
    sum: (a,b) => {
        return a+b;
    },

/**
 * Subtract
 * @param {Number} a 
 * @param {Number} b 
 * @returns {Number} a - b
 */
    subtract: (a, b) => {
        return a - b;
    }
}