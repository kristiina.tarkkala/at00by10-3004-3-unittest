const express = require('express')
const app = express()
const port = 3000
const mylib = require('./mylib')

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/add', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    console.log({a,b});

    if (a && b)
    {
        const sum = mylib.sum(a,b);
        res.send('total: ' + sum.toString());
    }
    else
    {
        res.send('Parameters invalid or missing - make sure only numbers are used.');
    }
});

app.get('/sub', (req, res) => {
    const a = parseInt(req.query.a);
    const b = parseInt(req.query.b);
    console.log({a,b});

    if (a && b)
    {
        const result = mylib.subtract(a,b);
        res.send('result: ' + result.toString());
    }
    else
    {
        res.send('Parameters invalid or missing - make sure only numbers are used.');
    }
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
